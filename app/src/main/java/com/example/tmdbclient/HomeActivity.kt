package com.example.tmdbclient

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.tmdbclient.databinding.ActivityHomeBinding



class HomeActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_home)
        mBinding.hanlder = this
    }

    fun navigateToMovieList(v: View){
//        val intent = Intent(this,MovieActivity::class.java)
//        startActivity(intent)
    }

    fun navigateToTvShowsList(v: View){
//        val intent = Intent(this,TvShowsActivity::class.java)
//        startActivity(intent)
    }

    fun navigateToArtistsList(v: View){
//        val intent = Intent(this,ArtistActivity::class.java)
//        startActivity(intent)
    }
    fun navigateToArtistsList1(v: View){
//        val intent = Intent(this,ArtistActivity::class.java)
//        startActivity(intent)
    }
}